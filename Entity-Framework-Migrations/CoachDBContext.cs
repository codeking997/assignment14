﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entity_Framework_Migrations
{
    public class CoachDBContext : DbContext
    {
        public DbSet<Coach> Coaches { get; set; }

        public DbSet<Athlet> athlets { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=PC7585\\SQLEXPRESS;Initial Catalog=CoachDB; Integrated Security=True;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Coach>().Property(e => e.AthletId).ValueGeneratedNever();
            modelBuilder.Entity<Coach>().Property(e => e.AthletId).ValueGeneratedNever();
        }
    }
}
