﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Entity_Framework_Migrations
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("to see the list press d, to remove press r, press a to add");
            string userInput = Console.ReadLine();
            if (userInput == "d")
            {
                display();
            }else if(userInput == "r")
            {
                RemoveAthlet();
            }else if (userInput == "a")
            {
                addCoach();
            }

            
        }
        /*
         * this method allows the user to create a new coach and a athlet as he pleases
         * it creates a connection then it uses user input to ask the user to provide detalis about
         * the new coach or athlet. when finished it saves the changes
         */
        private static void addCoach()
        {
            using (CoachDBContext coachDBContext = new CoachDBContext())
            {
                Console.WriteLine("write c for coach and a for athlet");
                string coachInput = Console.ReadLine();
                if (coachInput == "c")
                {
                    Console.WriteLine("enter a first name");
                    string firstName = Console.ReadLine();
                    Console.WriteLine("enter a last name");
                    string lastName = Console.ReadLine();
                    Console.WriteLine("enter a sport");
                    string sport = Console.ReadLine();
                    coachDBContext.Coaches.Add(new Coach { FirstName = firstName, LastName = lastName, Sport = sport });
                }
                else if (coachInput == "a")
                {
                    Console.WriteLine("enter a first name");
                    string firstName = Console.ReadLine();
                    Console.WriteLine("enter a last name");
                    string lastName = Console.ReadLine();
                    Console.WriteLine("enter a age");
                    int ageOfAthlet = Convert.ToInt32(Console.ReadLine());
                    coachDBContext.athlets.Add(new Athlet { FirstName = firstName, LastName = lastName, age = ageOfAthlet });
                }
                coachDBContext.SaveChanges();
                /*var coach = coachDBContext.Coaches.Include(s => s.athlet).SingleOrDefault(s => s.Id == 1);
                Console.WriteLine($"the name of the coach is {coach.FirstName} and he trains {coach.athlet.LastName}");*/
                

            }
        }
        /*
         * this method allows the user to display all the coaches and who they train
         * establishes a connection to the database
         * uses a foreach to go through the list of coaches. 
         * it then prints out the names of coaches
         * the if statement checks if there is a relation between athlet and coach
         * if not it will not run, if so it will show the different coaches and who they train.
         */
        private static void display()
        {
            using (CoachDBContext coachDBContext = new CoachDBContext())
            {
                var coaches = coachDBContext.Coaches.Select(s => s).Include(p => p.athlet);
                foreach (Coach coach in coaches) {
                   
                    Console.WriteLine($"{coach.FirstName} {coach.LastName}");
                    if (coach.athlet != null)
                    {
                        Console.WriteLine($"{coach.athlet.FirstName} {coach.athlet.LastName}");
                        Console.WriteLine($"the name of the coach is {coach.FirstName} {coach.LastName} and he coaches {coach.athlet.FirstName} {coach.athlet.LastName}");
                    }
                    
                }
                
            }
        }
        /*
         * this method allows the user to remove an athlet
         * works the same as the above mentioned methods.
         */
        private static void RemoveAthlet()
        {
            using (CoachDBContext coachDBContext = new CoachDBContext())
            {
                Console.WriteLine("enter a number to remove");
                int removeId = Convert.ToInt32(Console.ReadLine()); 
                var athlet = coachDBContext.athlets.Find(removeId);
                //var sportsmen = coachDBContext.athlets.Where(s => s.);
                coachDBContext.athlets.Remove(athlet);
                Console.WriteLine($"athlet was removed: {athlet.FirstName} {athlet.LastName}");
                coachDBContext.SaveChanges();
            }
        }
        
    }
}
